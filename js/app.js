function shadeColor1(color, percent) {
  var num = parseInt(color.slice(1),16), amt = Math.round(2.55 * percent), R = (num >> 16) + amt, G = (num >> 8 & 0x00FF) + amt, B = (num & 0x0000FF) + amt;
  return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (G<255?G<1?0:G:255)*0x100 + (B<255?B<1?0:B:255)).toString(16).slice(1);
}


function updateSubtotal() {
  // Recompute .price-breakdown
  // Update .price-subtotal td:last-child
}
function updateGrandtotal() {
}

$(window).on('load', function() {

  var undoManager = new UndoManager();

  $('.page-home .btn-create-your-own').on('click', function() {
    $(this).addClass('animate');
    $('.page-home').addClass('animate');
  });

  $('.options-style-color li a').each(function() {
    $(this).css('background-color', $(this).data('color'));
  });

  $('.js-carousel').owlCarousel({
    items: 4,
    navigation: true,
    pagination: false,
    responsive: false
  });


  $('.js-industrial-carousel li').owlCarousel({
    items: 1,
    navigation: true,
    pagination: false,
    responsive: false,
    singleItem: true
  });


  $('.options-header li a').on('click', function() {
    $('.options-item').each(function() {
      $(this).hide();
    });

    $('.options-header li a').each(function() {
      $(this).removeClass('active');
    });

    $(this).addClass('active');

    $($(this).data('open')).show();
  });

  $('.options-size ul li a, .js-carousel li a, .options-style-color li a').on('click', function() {
    $(this).parents('ul').find('li').each(function() {
      $(this).removeClass('active');
    });

    $(this).parent().addClass('active');
  });


  var pattern = $('.pattern').svg({
    loadURL: 'assets/slippers/pattern.svg'
  });

  function removeSelectedSlipper() {
    $('#slippers-container', svg.root()).removeAttr('class');
    $('#left, #right', svg.root()).each(function() {
      $(this).removeAttr('class');
    });
  }

  var slippers = $('.main-show-top').svg({
    loadURL: 'assets/slippers/slippers.svg',
    onLoad: function() {
      var x = slippers.svg('get');

      $(svg.root()).attr('height', '500')

      $('#left, #right', svg.root()).on('dblclick', function(e) {
        $('#slippers-container', svg.root()).attr('class', 'has-active');
        $('#left, #right', svg.root()).each(function() {
          $(this).removeAttr('class');
        });
        $(this).attr('class', 'active');

        e.stopPropagation();
      });

      $(svg.root()).on('dblclick', function() {
        removeSelectedSlipper();
      });
      var defs = svg.defs();
      var filter = svg.filter(defs, 'MultiplyFilter');
      svg.filters.image(filter, 'strap-left', '#strap-left');
      svg.filters.offset(filter, 'strap-left', 'strap-left', -4, -62);
      svg.filters.blend(filter, 'blended', 'multiply', 'SourceGraphic', 'strap-left');

    }
  });

  var slippersLeft = $('.main-show-left').svg({
    loadURL: 'assets/slippers/preview-left.svg',
    onLoad: function() {
      $(slippersLeftSvg.root()).attr('height', 100);
      $(slippersLeftSvg.root()).attr('style', 'margin-top: 150px');
    }
  });

  var slippersLeftSvg = slippersLeft.svg('get');

  var slippersRight = $('.main-show-right').svg({
    loadURL: 'assets/slippers/preview-left.svg',
    onLoad: function() {
      $(slippersRightSvg.root()).attr('height', 100);
      $(slippersRightSvg.root()).attr('style', 'margin-top: 150px');
    }
  });

  var slippersRightSvg = slippersRight.svg('get');

  var slippersPreviewLeft = $('.main-view-left').svg({
    loadURL: 'assets/slippers/preview-left.svg'
  });

  var slippersPreviewLeftSvg = slippersPreviewLeft.svg('get');

  var slippersPreviewRight = $('.main-view-right').svg({
    loadURL: 'assets/slippers/preview-left.svg',
    onLoad: function() {
      $('g', slippersPreviewRightSvg.root()).attr('transform', 'scale(-1, 1) translate(-612.4, 0)');
    }
  });

  var slippersPreviewRightSvg = slippersPreviewRight.svg('get');

  var slippersPreviewTop = $('.main-view-top').svg({
    loadURL: 'assets/slippers/preview-top.svg'
  });

  var slippersPreviewTopSvg = slippersPreviewTop.svg('get');

  slippers.wrapInner('<div class="panzoom"/>');
  slippersLeft.wrapInner('<div class="panzoom"/>');
  slippersRight.wrapInner('<div class="panzoom"/>');
  var panzoom = $('.panzoom').panzoom({
    $zoomIn: $('.btn-zoom-in'),
    $zoomOut: $('.btn-zoom-out'),
    increment: 0.1,
    $reset: $('.main-tools-zoom p'),
    onReset: function() {
      $('.main-tools-zoom p').text(100 + '%');
      currentZoom = 1;
    }

  });



  // Strap style
  $('.options-style ul:eq(0) li').on('click', function() {
    $('.options-style-color').each(function() { $(this).removeClass('active') });
    $('.options-style-color').eq(0).addClass('active');
  });

  $('.options-style-color:eq(0) li a').on('click', function() {
    var colorname = $(this).data('colorname');
    var basename;

    $('#strap-left-container image, #strap-right-container image').each(function() {
      basename = $(this).data('basename');

      console.log(basename);

      if (!basename) {
        basename = 'men1a';
      }

      $(this).data('colorname', colorname);
      $(this).attr('xlink:href', 'assets/slippers/../straps/' + basename + '-top-' + colorname + '.png');
    });

    $('#strap', slippersLeftSvg.root()).attr('xlink:href', 'assets/slippers/../straps/' + basename + '-side-' + colorname + '.png');
    $('#strap', slippersRightSvg.root()).attr('xlink:href', 'assets/slippers/../straps/' + basename + '-side-' + colorname + '.png');
  });

  // Sole style
  $('.options-style ul:eq(1) li').on('click', function() {
    $('.options-style-color').each(function() { $(this).removeClass('active') });
    $('.options-style-color').eq(1).addClass('active');

  });

  $('.options-style-color:eq(1) li a').on('click', function() {
    var color = $(this).data('color');
    modify.soleColor(color);
  });

  $('.options-design-print-design li a').each(function() {
    var image = $(this).data('image');
    $(this).parent().css('background-image', 'url(' + image + ')');

    $(this).on('click', function() {
      var imageObj = new Image();
      imageObj.onload = function() {
        var width = this.width;
        var height = this.height;

        if ($('.active', svg.root()).length) {
          var active = $('.active', svg.root());
          var x = active.attr('id');
          $('#printdesign-' + x +' image').each(function() { $(this).remove(); });

          $('#printdesign-' + x).each(function() { $(this).attr('height', height).attr('width', width) });

          svg.image($('#printdesign-' + x), 0, 0, width, height, image);


          if (x == 'left') {
            $('#front_left', svg.root()).attr('fill', 'url("#printdesign-left")');
            $('#preview-left-front', slippersLeftSvg.root()).attr('fill', 'url("#printdesign-left")');
          }

          if (x == 'right') {
            $('#front_right', svg.root()).attr('fill', 'url("#printdesign-right")');
            $('#preview-left-front', slippersRightSvg.root()).attr('fill', 'url("#printdesign-right")');
          }

        }

        else {
          $('#printdesign image, #printdesign-left image, #printdesign-right image').each(function() { $(this).remove(); });
          $('#printdesign, #printdesign-left, #printdesign-right').each(function() { $(this).attr('height', height).attr('width', width) });
          svg.image($('#printdesign-left'), 0, 0, width, height, image);
          svg.image($('#printdesign-right'), 0, 0, width, height, image);
          svg.image($('#printdesign'), 0, 0, width, height, image);
          $('#front_left', svg.root()).attr('fill', 'url("#printdesign-left")');
          $('#front_right', svg.root()).attr('fill', 'url("#printdesign-right")');
          $('#preview-left-front', slippersLeftSvg.root()).attr('fill', 'url("#printdesign-left")');
          $('#preview-left-front', slippersRightSvg.root()).attr('fill', 'url("#printdesign-right")');
        }



      };

      imageObj.src = image;

    });
  });

  $('.btn-mirror').on('click', function() {
    /* Pseudo code

    Get active slipper

    Get active slipper pattern

    Copy image on  opposite slipper pattern

    Copy attributes (height width transform)

    Perform transform to mirror

    Set opposite slipper transformed pattern

     */

    if ($('.active', svg.root()).length) {
      var active = $('.active', svg.root());
      var x = active.attr('id');

      if (x == 'left' || x == 'right') {

        $('#front_left', svg.root()).attr('fill', 'url("#printdesign-left")');
        $('#front_right', svg.root()).attr('fill', 'url("#printdesign-right")');
        $('#preview-left-front', slippersLeftSvg.root()).attr('fill', 'url("#printdesign-left")');
        $('#preview-left-front', slippersRightSvg.root()).attr('fill', 'url("#printdesign-right")');

        var patternSource;
        var patternDestination;

        if (x == 'left') {
          patternSource = $('#printdesign-left');
          patternDestination = $('#printdesign-right');
        }
        else if (x == 'right') {
          patternSource = $('#printdesign-right');
          patternDestination = $('#printdesign-left');
        }

        var pattern = patternSource.find('image').clone();

        patternDestination.find('image').remove();

        var w = patternSource.attr('width');
        var h = patternSource.attr('height');
        var px = patternSource.attr('x');
        var py = patternSource.attr('y');
        var pr = patternSource[0].getAttribute("patternTransform");

        patternDestination.attr('x', -(parseInt(px)) + 450);
        patternDestination.attr('y', py);
        patternDestination.attr('width', w);
        patternDestination.attr('height', h);
        if (pr) { patternDestination[0].setAttribute('patternTransform', pr); }

        $(pattern).attr('transform', "scale(-1,1) translate(-" + w + ",0)");

        $(pattern).appendTo(patternDestination);

      }

    }

    else {
      alert('Please select a slipper');
    }

  });

  $('.btn-remove-upload').on('click', function() {
  });

  var currentZoom = 1;

  $('.btn-zoom-out').on('click', function() {
    currentZoom -= 0.1;
    modify.zoom(currentZoom);
  });

  $('.btn-zoom-in').on('click', function() {
    currentZoom += 0.1;
    modify.zoom(currentZoom);
  });

  $('#upload').click(function(e) {
    e.stopPropagation();
  });

  $('.btn-upload').on('click', function() {
    $('#upload').click();
  });

  window.previewImage = function(img) {
    if (img.files && img.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        var image = new Image();
        image.onload = function () {

          var width = this.width;
          var height = this.height;

          var image = e.target.result;


          if ($('.active', svg.root()).length) {
            var active = $('.active', svg.root());
            var x = active.attr('id');
            $('#printdesign-' + x +' image').each(function() { $(this).remove(); });

            $('#printdesign-' + x).each(function() { $(this).attr('height', height).attr('width', width) });

            svg.image($('#printdesign-' + x), 0, 0, width, height, image);


            if (x == 'left') {
              $('#front_left', svg.root()).attr('fill', 'url("#printdesign-left")');
              $('#preview-left-front', slippersLeftSvg.root()).attr('fill', 'url("#printdesign-left")');
            }

            if (x == 'right') {
              $('#front_right', svg.root()).attr('fill', 'url("#printdesign-right")');
              $('#preview-left-front', slippersRightSvg.root()).attr('fill', 'url("#printdesign-right")');
            }

          }

          else {
            $('#printdesign image, #printdesign-left image, #printdesign-right image').each(function() { $(this).remove(); });
            $('#printdesign, #printdesign-left, #printdesign-right').each(function() { $(this).attr('height', height).attr('width', width) });
            svg.image($('#printdesign-left'), 0, 0, width, height, image);
            svg.image($('#printdesign-right'), 0, 0, width, height, image);
            svg.image($('#printdesign'), 0, 0, width, height, image);
            $('#front_left', svg.root()).attr('fill', 'url("#printdesign-left")');
            $('#front_right', svg.root()).attr('fill', 'url("#printdesign-right")');
            $('#preview-left-front', slippersLeftSvg.root()).attr('fill', 'url("#printdesign-left")');
            $('#preview-left-front', slippersRightSvg.root()).attr('fill', 'url("#printdesign-right")');
          }


        }
        image.src = e.target.result;
      }
      reader.readAsDataURL(img.files[0]);
    }
  }



  var svg = slippers.svg('get');

  var modify = {
    zoom: function(x) {
      //$('#slippers-container', svg.root()).attr('transform', 'scale(' + x + ',' + x + ')' );
      $('.main-tools-zoom p').text(Math.floor(currentZoom * 100) + '%');
    },

    soleColor: function(x) {
      $('#front_left, #front_right', svg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#bottom_left, #bottom_right', svg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -25));
      });

      // Main Right
      $('#preview-left-front', slippersRightSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-left-bottom', slippersRightSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -25));
      });

      // Main Left
      $('#preview-left-front', slippersLeftSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-left-bottom', slippersLeftSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -25));
      });

      // Preview Right
      $('#preview-left-front', slippersPreviewRightSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-left-bottom', slippersPreviewRightSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -25));
      });

      // Preview Left
      $('#preview-left-front', slippersPreviewLeftSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-left-bottom', slippersPreviewLeftSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -25));
      });

      // Preview Top
      $('#preview-top-sole', slippersPreviewTopSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-top-sole-bottom', slippersPreviewTopSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -25));
      });
    },

    strapColor: function(x) {
      $('#strap-left, #strap-right', svg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#strap-highlight-right path, #strap-connector-right, #strap-highlight-left path, #strap-connector-left', svg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -15));
      });

      // Main Right
      $('#preview-left-strap-front', slippersRightSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-left-strap-bottom, #preview-left-strap-connector', slippersRightSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -15));
      });

      // Main Left
      $('#preview-left-strap-front', slippersLeftSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-left-strap-bottom, #preview-left-strap-connector', slippersLeftSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -15));
      });

      // Preview Right
      $('#preview-left-strap-front', slippersPreviewRightSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-left-strap-bottom, #preview-left-strap-connector', slippersPreviewRightSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -15));
      });

      // Preview Left
      $('#preview-left-strap-front', slippersPreviewLeftSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-left-strap-bottom, #preview-left-strap-connector', slippersPreviewLeftSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -15));
      });

      // Preview Top
      $('#preview-top-strap', slippersPreviewTopSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-top-strap-shadow path', slippersPreviewTopSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -15));
      });
    },

    uploadZoom: function(x) {
      if ($('.active', svg.root()).length) {
        var active = $('.active', svg.root());
        var activeId = active.attr('id');
        $('#printdesign-' + activeId).attr('patternTransform', 'scale(' + x + ',' + x + ')' );
      }
      else {
        $('#printdesign-left, #printdesign-right').attr('patternTransform', 'scale(' + x + ',' + x + ')' );
      }
    },

    uploadMove: function(direction) {
      if ($('.active', svg.root()).length) {
        var active = $('.active', svg.root());
        var x = active.attr('id');
        var currentX = parseInt($('#printdesign-' + x).attr('x'));
        var currentY = parseInt($('#printdesign-' + x).attr('y'));
        if (direction == 'up') {
          $('#printdesign-' + x).attr('y', currentY - 20 );
        }
        if (direction == 'down') {
          $('#printdesign-' + x).attr('y', currentY + 20 );
        }
        if (direction == 'left') {
          $('#printdesign-' + x).attr('x', currentX - 20 );
        }
        if (direction == 'right') {
          $('#printdesign-' + x).attr('x', currentX + 20 );
        }
      }
      else {
        var currentX = parseInt($('#printdesign-left, #printdesign-right').attr('x'));
        var currentY = parseInt($('#printdesign-left, #printdesign-right').attr('y'));
        if (direction == 'up') {
          $('#printdesign-left, #printdesign-right').attr('y', currentY - 20 );
        }
        if (direction == 'down') {
          $('#printdesign-left, #printdesign-right').attr('y', currentY + 20 );
        }
        if (direction == 'left') {
          $('#printdesign-left, #printdesign-right').attr('x', currentX - 20 );
        }
        if (direction == 'right') {
          $('#printdesign-left, #printdesign-right').attr('x', currentX + 20 );
        }
      }
    }

  }

  var uploadZoomValue = 1;

  $('.btn-upload-zoom-out').on('click', function() {
    uploadZoomValue -= 0.1;
    modify.uploadZoom(uploadZoomValue);
  });

  $('.btn-upload-zoom-in').on('click', function() {
    uploadZoomValue += 0.1;
    modify.uploadZoom(uploadZoomValue);
  });
  $('.btn-upload-move-up').on('click', function() {
    modify.uploadMove('up');
  });
  $('.btn-upload-move-down').on('click', function() {
    modify.uploadMove('down');
  });
  $('.btn-upload-move-left').on('click', function() {
    modify.uploadMove('left');
  });
  $('.btn-upload-move-right').on('click', function() {
    modify.uploadMove('right');
  });

  $('.btn-close, .btn-skip, .btn-done, .js-close-popup').on('click', function() {
    $(this).parent().removeClass('active');
  });

  $('.popup-signin .btn-register').on('click', function() {
    $('.popup-signin').removeClass('active');
    $('.popup-register').addClass('active');
  });

  $('.btn-gallery').on('click', function() {
    $('.popup-gallery').addClass('active');
  });

  $('.popup-gallery .item').on('click', function() {
    $('.popup-gallery .btn-close').click();
  });

  $('.btn-login, .btn-signup').on('click', function() {
    $('.popup-signin').addClass('active');
  });

  $('.btn-preview').on('click', function() {
    $('.popup-checkout .btn-checkout-final').text('Save');
    $('.popup-checkout .checkout-slipper-container').html($('.main-show svg').clone());
    $('.popup-checkout .checkout-slipper-container svg').eq(0).addClass('active');
    $('.popup-checkout').addClass('active');
  });
  $('.btn-checkout').on('click', function() {
    $('.popup-checkout .btn-checkout-final').text('Checkout');
    $('.popup-checkout .checkout-slipper-container').html($('.main-show svg').clone());
    $('.popup-checkout .checkout-slipper-container svg').eq(0).addClass('active');
    $('.popup-checkout').addClass('active');
  });

  $('.popup-checkout .btn-next').on('click', function() {
    if ($('.popup-checkout .checkout-slipper-container .active').next().length) {
      $('.popup-checkout .checkout-slipper-container .active').removeClass('active').hide().next().addClass('active').show();
    }
    else {
      $('.popup-checkout .checkout-slipper-container .active').removeClass('active');
      $('.popup-checkout .checkout-slipper-container svg').eq(0).show().addClass('active');
    }
  });

  $('.popup-checkout .btn-prev').on('click', function() {
    if ($('.popup-checkout .checkout-slipper-container .active').prev().length) {
      $('.popup-checkout .checkout-slipper-container .active').hide().removeClass('active').prev().addClass('active').show();
    }
    else {
      $('.popup-checkout .checkout-slipper-container .active').hide().removeClass('active').hide();
      $('.popup-checkout .checkout-slipper-container svg').eq(-1).addClass('active').show();
    }
  });

  $('.btn-checkout-final').on('click', function() {
    $('.popup-checkout').removeClass('active');
    $('.popup-get-ticket').addClass('active');
  });

  $('.options-style-strap li a').on('click', function() {
    var basename = $(this).data('basename');

    $('#strap-left-container image, #strap-right-container image').each(function() {
      $(this).attr('xlink:href', 'assets/slippers/../straps/' + basename + '-top-green.png');
      $(this).data('basename', basename);

    });

    $('#strap', slippersLeftSvg.root()).attr('xlink:href', 'assets/slippers/../straps/' + basename + '-side-' + color + '.png');
    $('#strap', slippersRightSvg.root()).attr('xlink:href', 'assets/slippers/../straps/' + basename + '-side-' + color + '.png');

  });


  function hideShows() {
    $('.main-show > div').each(function() { $(this).hide(); });
    $('.main-view > div').each(function() { $(this).removeClass('active') });
  };

  $('.main-show-top').show();

  $('.main-view-left').on('click', function() {
    hideShows()
    $('.main-show-left').show();
    $(this).addClass('active');


    $('#left, #right', svg.root()).each(function() {
      $(this).removeAttr('class');
    });
    $('#left', svg.root()).attr('class', 'active');
  });
  $('.main-view-top').on('click', function() {
    hideShows()
    $('.main-show-top').show();
    $(this).addClass('active');
    removeSelectedSlipper();
  });
  $('.main-view-right').on('click', function() {
    hideShows()
    $('.main-show-right').show();
    $(this).addClass('active');
    $('#left, #right', svg.root()).each(function() {
      $(this).removeAttr('class');
    });
    $('#right', svg.root()).attr('class', 'active');
  });

  $('.js-keyboard').onScreenKeyboard();


  $('.options-strap-accessories li a').on('click', function() {
    var img = $(this).find('img').attr('src');
    var price = $(this).data('price');
    $('.popup-accessory div img').attr('src', img);
    $('.popup-accessory .currency strong').text(price);

    $('.popup-accessory').addClass('active');
  });


  var accessory = null;

  var currentAccessory;

  $('.js-add-accessory').on('click', function() {

    /*
    Get svg root
    get image source
    add to left slipper
    disable panzoom
    make image draggable
    limit image draggable
     */

    $(this).parents('.popup').removeClass('active');

    $('.main-place-accessory.cover').addClass('active');

    $('.main-view-top').click();

    var imageSource = $('.popup-accessory').find('img').attr('src');

    accessory = svg.image($('#left', svg.root()), 13, 155, 40, 40, imageSource);

    var path = $('#strap-left-path', svg.root())[0];

    $(panzoom).each(function() {
      $(this).panzoom('reset');
      $(this).panzoom('disable');
    });

    var gradSearch = function (l0, pt) {
      l0 = l0 + totLen;
      var l1 = l0,
        dist0 = dist(path.getPointAtLength(l0 % totLen), pt),
        dist1,
        searchDir;

      if (dist(path.getPointAtLength((l0 - searchDl) % totLen), pt) >
        dist(path.getPointAtLength((l0 + searchDl) % totLen), pt)) {
        searchDir = searchDl;
      } else {
        searchDir = -searchDl;
      }

      l1 += searchDir;
      dist1 = dist(path.getPointAtLength(l1 % totLen), pt);
      while (dist1 < dist0) {
        dist0 = dist1;
        l1 += searchDir;
        dist1 = dist(path.getPointAtLength(l1 % totLen), pt);
      }
      l1 -= searchDir;
      return (l1 % totLen);
    };

    var dist = function (pt1, pt2) {
      var dx = pt1.x - pt2.x;
      var dy = pt1.y - pt2.y;
      return Math.sqrt(dx * dx + dy * dy);
    };

    var l = 0;
    var searchDl = 1;
    var totLen = path.getTotalLength();

    $(accessory).draggable()
      .bind('drag', function(event, ui) {

        var mouseX = event.clientX - $('.main-show-top').offset().left;
        var container = $('.main-show-top').width();

        if (mouseX < container/2) {
          path = $('#strap-left-path', svg.root())[0];
          $(accessory).appendTo('#left');
        }

        else {
          path = $('#strap-right-path', svg.root())[0];
          $(accessory).appendTo('#right');
        }

        var $this = accessory;
        var whichDrag = $this;
        var tmpPt = {
          x : event.clientX - $('.main-show-top').offset().left - 150,
          y : event.clientY - $('.main-show-top').offset().top - 50
        };

        l = gradSearch(l, tmpPt);
        pt = path.getPointAtLength(l);

        //$(accessory).attr({x: pt.x, y: pt.y});
        $(accessory).attr({x: pt.x - 20, y: pt.y - 20});

      })
      .on('mouseover', function() {
        var x = $(this).offset().top;
        var y = $(this).offset().left;
        $('.accessory-close').css({top: x, left: y}).addClass('active');

        currentAccessory = $(this);

      })
      .on('mouseout', function() {
        $('.accessory-close').removeClass('active');
      });
  });

  $('.accessory-close').on('click', function() {
    $(currentAccessory).remove();
    $(this).removeClass('active');

  });
  $('.accessory-close').on('mouseover', function() {
    $(this).addClass('active');

  });
  $('.main-place-accessory .btn-cancel').on('click', function() {
    $(panzoom).each(function() {
      $(this).panzoom('enable');
    });
    $('.main-place-accessory.cover').removeClass('active');
    $(accessory).remove();

  });
  $('.main-place-accessory .btn-done').on('click', function() {
    $(panzoom).each(function() {
      $(this).panzoom('enable');
    });
    $('.main-place-accessory.cover').removeClass('active');
    $(accessory).draggable('destroy').off('drag');
  });

  $('.btn-undo').on('click', function() {
    undoManager.undo();
    console.log('undo');
  });

  $('.btn-redo').on('click', function() {
    undoManager.redo();
  });

  $('.theme-choices .btn').on('click', function() {
    $('body').addClass($(this).data('theme'));
  });

});

$(window).on('load', function() {

  var themeChoicesSlider = $('.theme-choices').bxSlider({
    minSlides: 2,
    maxSlides: 2,
    slideWidth: 400,
    pager: true,
    controls: false
  });

  $('.page-choose-theme .bx-next').on('click', function() {
    themeChoicesSlider.goToNextSlide();
  });
  $('.page-choose-theme .bx-prev').on('click', function() {
    themeChoicesSlider.goToPrevSlide();
  });

  $('.page-choose-theme li .btn').on('click', function() {
    $('.page-choose-theme').fadeOut();
  });

});
$(window).on('load', function() {
  // Show page on-load only
  setTimeout(function() {
    $('body').removeClass('hide');
  }, 500);

  $('.page-start a').on('click', function() {
    $('body').prepend('<div class="transition-to-black" />');
    setTimeout(function() {
      var $el = $('.transition-to-black').addClass('active');
      setTimeout(function() {
        $('.page-start').hide();
        $el.addClass('unactive');
      }, 500);
      setTimeout(function() {
        $el.removeClass('active unactive');
        $('.transition-to-black').remove();
      }, 750);
    }, 200);
  });


});
