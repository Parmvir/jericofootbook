$(window).on('load', function() {

  var themeChoicesSlider = $('.theme-choices').bxSlider({
    minSlides: 2,
    maxSlides: 2,
    slideWidth: 400,
    pager: true,
    controls: false
  });

  $('.page-choose-theme .bx-next').on('click', function() {
    themeChoicesSlider.goToNextSlide();
  });
  $('.page-choose-theme .bx-prev').on('click', function() {
    themeChoicesSlider.goToPrevSlide();
  });

  $('.page-choose-theme li .btn').on('click', function() {
    $('.page-choose-theme').fadeOut();
  });

});