$(window).on('load', function() {
  // Show page on-load only
  setTimeout(function() {
    $('body').removeClass('hide');
  }, 500);

  $('.page-start a').on('click', function() {
    $('body').prepend('<div class="transition-to-black" />');
    setTimeout(function() {
      var $el = $('.transition-to-black').addClass('active');
      setTimeout(function() {
        $('.page-start').hide();
        $el.addClass('unactive');
      }, 500);
      setTimeout(function() {
        $el.removeClass('active unactive');
        $('.transition-to-black').remove();
      }, 750);
    }, 200);
  });


});
