var gulp = require('gulp');

var autoprefixer = require('gulp-autoprefixer');
var stylus = require('gulp-stylus');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var order = require('gulp-order');
var jeet = require('jeet');
var rupture = require('rupture');

gulp.task('vendorjs', function() {
  gulp.src(['js/lib/*.js'])
    .pipe(order([
      "jquery.js",
      "jquery-ui.js",
    ]))
    .pipe(uglify())
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('js'))
});

gulp.task('appjs', function() {
  gulp.src(['js/app/*.js'])
    .pipe(concat('app.js'))
    .pipe(gulp.dest('js'))
});


gulp.task('stylus', function() {
  gulp.src(['css/style.styl'])
    .pipe(stylus({ use: [ jeet(), rupture() ] }))
    .pipe(autoprefixer())
    .pipe(gulp.dest('./'))
});

gulp.task('watch', function() {
  gulp.watch(['css/*.styl', 'css/styl/*.styl'], ['stylus']);
  gulp.watch(['js/lib/*.js'], ['vendorjs']);
  gulp.watch(['js/app/*.js'], ['appjs']);
});

gulp.task('default', ['stylus']);
